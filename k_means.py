#!/usr/bin/python

def extract_fields(line):
	user_id=''
	age_gen=''
	occup=''
	zip_code=''
	prefer_movies=''
	average_movies=''
	bad_movies=''
	summary_movie=''
	above_below=''
	experience=''
	index=0
	while(line[index]!='|'):
		user_id=user_id+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		age_gen=age_gen+line[index]
		index=index+1
	index=index+1
	
	while(line[index]!='|'):
		occup=occup+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		zip_code=zip_code+line[index]
		index=index+1
	index=index+1
	
	
	while(line[index]!='|'):
		prefer_movies=prefer_movies+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		average_movies=average_movies+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		bad_movies=bad_movies+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		summary_movie=summary_movie+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		above_below=above_below+line[index]
		index=index+1
	index=index+1


	while(line[index]!='\n'):
		experience=experience+line[index]
		index=index+1
	
	return user_id,age_gen,occup,zip_code,prefer_movies,average_movies,bad_movies,summary_movie,above_below,experience

def age_gender_function(age_gen):
	if(age_gen==0):
		return (-1,1)
	elif(age_gen==1):
		return (-1,2)
	elif(age_gen==2):
		return (-1,3)
	elif(age_gen==3):
		return (-1,4)
	elif(age_gen==4):
		return (-1,5)
	elif(age_gen==5):
		return (-1,6)
	elif(age_gen==6):
		return (1,1)
	elif(age_gen==7):
		return (1,2)
	elif(age_gen==8):
		return (1,3)
	elif(age_gen==9):
		return (1,4)
	elif(age_gen==10):
		return (1,5)
	elif(age_gen==11):
		return (1,6)

def occupation_function(occup):
	if(occup==0):
		return (1,4)
	elif(occup==1):
		return (-3,-4)
	elif(occup==2):
		return (-3,3)
	elif(occup==3):
		return (2,3)
	elif(occup==4):
		return (0,0)
	elif(occup==5):
		return (2,-2)
	elif(occup==6):
		return (3,1)
	elif(occup==7):
		return (-2,1)
	elif(occup==8):
		return (0,1)
	elif(occup==9):
		return (1,-1)
	elif(occup==10):
		return (-2,-2)
	elif(occup==11):
		return (-3,-3)
	
def movies_function(movies):
	
	if(movies==0) or (movies==-1):
		return(0,0)
	elif(movies==1):
		return(9,9)
	elif(movies==2):
		return(8,8)
	elif(movies==3):
		return(7,7)
	elif(movies==4):
		return(6,6)
	elif(movies==5):
		return(5,5)
	elif(movies==6):
		return(4,4)
	elif(movies==7):
		return(3,3)
	elif(movies==8):
		return(2,2)
	elif(movies==9):
		return(1,1)
	elif(movies==10):
		return(-1,1)
	elif(movies==11):
		return(-2,2)
	elif(movies==12):
		return(-3,3)
	elif(movies==13):
		return(-4,4)
	elif(movies==14):
		return(-5,5)
	elif(movies==15):
		return(-6,6)
	elif(movies==16):
		return(-7,7)
	elif(movies==17):
		return(-8,8)
	elif(movies==18):
		return(-9,9)


with open("user_vectors.txt") as users,open("Euclidean_parametres.txt","w") as euclidean:
	for data in users: 
		user_id,age_gen,occup,zip_code,prefer_movies,average_movies,bad_movies,summary_movie,above_below,experience=extract_fields(data)
		euclidean.write(user_id+'|')	
		age_gen_point=age_gender_function(int(age_gen))
		euclidean.write(str(age_gen_point[0])+',')
		euclidean.write(str(age_gen_point[1])+'|')	
		occup_point=occupation_function(int(occup))
		euclidean.write(str(occup_point[0])+',')
		euclidean.write(str(occup_point[1])+'|')
		euclidean.write(zip_code+'|')
		prefer_movies_point=movies_function(int(prefer_movies))
		euclidean.write(str(prefer_movies_point[0])+',')
		euclidean.write(str(prefer_movies_point[1])+'|')
		average_movies_point=movies_function(int(average_movies))
		euclidean.write(str(average_movies_point[0])+',')
		euclidean.write(str(average_movies_point[1])+'|')
		bad_movies_point=movies_function(int(bad_movies))
		euclidean.write(str(bad_movies_point[0])+',')
		euclidean.write(str(bad_movies_point[1])+'|')
		summary_movie_point=movies_function(int(summary_movie))
		euclidean.write(str(summary_movie_point[0])+',')
		euclidean.write(str(summary_movie_point[1])+'|')
		euclidean.write(above_below+'|')
		euclidean.write(experience+'\n')
