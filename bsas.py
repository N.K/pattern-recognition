#!/usr/bin/python
import math
from random import shuffle

def extract_fields(line):
	user_id=''
	age_gen=''
	occup=''
	zip_code=''
	prefer_movies=''
	average_movies=''
	bad_movies=''
	summary_movie=''
	above_below=''
	experience=''
	index=0
	while(line[index]!='|'):
		user_id=user_id+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		age_gen=age_gen+line[index]
		index=index+1
	index=index+1
	
	while(line[index]!='|'):
		occup=occup+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		zip_code=zip_code+line[index]
		index=index+1
	index=index+1
	
	
	while(line[index]!='|'):
		prefer_movies=prefer_movies+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		average_movies=average_movies+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		bad_movies=bad_movies+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		summary_movie=summary_movie+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		above_below=above_below+line[index]
		index=index+1
	index=index+1


	while(line[index]!='\n'):
		experience=experience+line[index]
		index=index+1
	
	return user_id,age_gen,occup,zip_code,prefer_movies,average_movies,bad_movies,summary_movie,above_below,experience


def hamming_distance(centroid,user_vector):	
	disimilarity_measure=0
	for x in range(len(centroid)-1):
		if(centroid[x+1]!=user_vector[x+1]) and (x+1==1):
			disimilarity_measure=disimilarity_measure+0.7
		elif(centroid[x+1]!=user_vector[x+1]) and (x+1==2):
			disimilarity_measure=disimilarity_measure+0.5
		elif(centroid[x+1]!=user_vector[x+1]) and (x+1==3):
			disimilarity_measure=disimilarity_measure+0.3
		elif(centroid[x+1]!=user_vector[x+1]) and (x+1<=7):
			disimilarity_measure=disimilarity_measure+1
		elif(centroid[x+1]!=user_vector[x+1]) and (x+1==8):
			disimilarity_measure=disimilarity_measure+0.5
		elif(centroid[x+1]!=user_vector[x+1]) and (x+1==9):
			disimilarity_measure=disimilarity_measure+0.6
	return disimilarity_measure

with open('user_vectors.txt') as lala_ku:
	
	list_of_vectors=[]
	lines=lala_ku.readline()
	user_id,age_gen,occup,zip_code,prefer_movies,average_movies,bad_movies,summary_movie,above_below,experience=extract_fields(lines)
	user_vector= (user_id,age_gen,occup,zip_code,prefer_movies,average_movies,bad_movies,summary_movie,above_below,experience)
	group={}
	group[user_vector]=[user_vector]
	for lines in lala_ku: 	 	
		user_vector=extract_fields(lines)
		min_distance=10
		min_centroid=''
		for centroids in group:
			disimilarity_measure=hamming_distance(centroids,user_vector)
			if(disimilarity_measure<min_distance):
				min_distance=disimilarity_measure
				min_centroid=centroids
		if(min_distance>3) and (len(group)<58):
			group[user_vector]=[user_vector]
		else:			
			group[min_centroid].append(user_vector)
			new_centroid=()
			centr_dict_age_gender={}
			centr_dict_occ={}
			centr_dict_zip_code={}
			centr_dict_preferable={}
			centr_dict_medium={}
			centr_dict_bad={}
			centr_dict_total={}
			centr_dict_above_below={}
			centr_dict_max_experience={}			
			for users in group[min_centroid]:
				tup_user_id,tup_age_gender,tup_occ,tup_zip_code,tup_preferable,tup_medium,tup_bad,tup_total,tup_above_below,tup_exp=users
				try:
					centr_dict_age_gender[tup_age_gender]=centr_dict_age_gender[tup_age_gender]+1
				except:
					centr_dict_age_gender[tup_age_gender]=1
				try:
					centr_dict_occ[tup_occ]=centr_dict_occ[tup_occ]+1
				except:
					centr_dict_occ[tup_occ]=1
				try:
					centr_dict_zip_code[tup_zip_code]=centr_dict_zip_code[tup_zip_code]+1
				except:
					centr_dict_zip_code[tup_zip_code]=1
				try:
					centr_dict_preferable[tup_preferable]=centr_dict_preferable[tup_preferable]+1
				except:
					centr_dict_preferable[tup_preferable]=1
				try:
					centr_dict_medium[tup_medium]=centr_dict_medium[tup_medium]+1
				except:
					centr_dict_medium[tup_medium]=1
				try:
					centr_dict_bad[tup_bad]=centr_dict_bad[tup_bad]+1
				except:
					centr_dict_bad[tup_bad]=1
				try:
					centr_dict_total[tup_total]=centr_dict_total[tup_total]+1
				except:
					centr_dict_total[tup_total]=1
				try:
					centr_dict_above_below[tup_above_below]=centr_dict_above_below[tup_above_below]+1
				except:
					centr_dict_above_below[tup_above_below]=1
				try:
					centr_dict_max_experience[tup_exp]=centr_dict_max_experience[tup_exp]+1
				except:
					centr_dict_max_experience[tup_exp]=1
			
			
			new_centroid=new_centroid+('0',)
			new_centroid=new_centroid+(max(centr_dict_age_gender, key=centr_dict_age_gender.get),)	
			new_centroid=new_centroid+(max(centr_dict_occ, key=centr_dict_occ.get),)
			new_centroid=new_centroid+(max(centr_dict_zip_code, key=centr_dict_zip_code.get),)
			new_centroid=new_centroid+(max(centr_dict_preferable, key=centr_dict_preferable.get),)
			new_centroid=new_centroid+(max(centr_dict_medium, key=centr_dict_medium.get),)
			new_centroid=new_centroid+(max(centr_dict_bad, key=centr_dict_bad.get),)
			new_centroid=new_centroid+(max(centr_dict_total, key=centr_dict_total.get),)
			new_centroid=new_centroid+(max(centr_dict_above_below, key=centr_dict_above_below.get),)
			new_centroid=new_centroid+(max(centr_dict_max_experience, key=centr_dict_max_experience.get),)
			if(new_centroid!=min_centroid):
				group[new_centroid]=group[min_centroid]
				del group[min_centroid]
	print("TH:{0}".format(3))	
	print("Nr of groups:{0}".format(len(group)))
	print('-------------------------------')
	with open("task_2_grouping.txt","w") as result:
		for centroids in group:
			result.write("Centroid:{0}\n".format(centroids))
			for users in group[centroids]:
				result.write(str(users)+'\n')
			result.write("-------------------------\n") 	 
	

