#!/usr/bin/python
import psycopg2
import sys


#Define our connection string
conn_string = "host='localhost' dbname='MovieLens_100K_Dataset' user='postgres' password='%CLASSIFIED'"
 
# print the connection string we will use to connect
#print ("Connecting to database\n{0}".format(conn_string))

# get a connection, if a connect cannot be made an exception will be raised here
conn = psycopg2.connect(conn_string)
 
# conn.cursor will return a cursor object, you can use this cursor to perform queries
cursor = conn.cursor()

print ("Connected!\n")

above_sea_level_movie_gendres=['is_fantasy','is_action','is_adventure','is_thriller','is_mystery','is_crime','is_drama','is_war','is_horror']
below_sea_level_movie_gendres=['is_animation','is_documentary','is_sci_fi','is_western','is_film_noir','is_romance','is_musical','is_comedy','is_childrens']
genre_list=['is_unknown','is_horror','is_war','is_drama','is_crime','is_mystery','is_thriller','is_adventure','is_action','is_fantasy','is_animation','is_documentary','is_sci_fi','is_western','is_film_noir','is_romance','is_musical','is_comedy','is_childrens']


users={}
sql_return={}

def extract_genre(user_id,rating_condition):
	#movie preferences
	# execute our Query
	statement="select *"
	statement=statement+" from movie_info inner join ratings on (item_id=movie_id)"
	statement=statement+" where ratings.user_id='"+user_id+"'"+rating_condition	
	cursor.execute(statement)
	# retrieve the records from the database
	preferable_genres=[]
	db_recs = cursor.fetchall()
	for x in db_recs:
		truth_gendre_dict={}		
		truth_gendre_dict['is_unknown']=x[5]
		truth_gendre_dict['is_action']=x[6]
		truth_gendre_dict['is_adventure']=x[7]
		truth_gendre_dict['is_animation']=x[8]
		truth_gendre_dict['is_childrens']=x[9]
		truth_gendre_dict['is_comedy']=x[10]
		truth_gendre_dict['is_crime']=x[11]
		truth_gendre_dict['is_documentary']=x[12]
		truth_gendre_dict['is_drama']=x[13]
		truth_gendre_dict['is_fantasy']=x[14]
		truth_gendre_dict['is_film_noir']=x[15]
		truth_gendre_dict['is_horror']=x[16]
		truth_gendre_dict['is_musical']=x[17]
		truth_gendre_dict['is_mystery']=x[18]
		truth_gendre_dict['is_romance']=x[19]
		truth_gendre_dict['is_sci_fi']=x[20]
		truth_gendre_dict['is_thriller']=x[21]
		truth_gendre_dict['is_war']=x[22]
		truth_gendre_dict['is_western']=x[23]
		movie_genre_distance=[],[]
		for item in truth_gendre_dict:
			if (truth_gendre_dict[item]==True):
				movie_genre_distance[0].append(item)
				movie_genre_distance[1].append(0)
		above_sea=0
		below_sea=0
		for spec in range(len(movie_genre_distance[0])):
			if movie_genre_distance[0][spec] in above_sea_level_movie_gendres:
				above_sea=above_sea+1
				movie_genre_distance[1][spec]=movie_genre_distance[1][spec]+1+above_sea_level_movie_gendres.index(movie_genre_distance[0][spec])
			elif movie_genre_distance[0][spec] in below_sea_level_movie_gendres:
				below_sea=below_sea+1
				movie_genre_distance[1][spec]=movie_genre_distance[1][spec]+1+below_sea_level_movie_gendres.index(movie_genre_distance[0][spec])
			else:
				movie_genre_distance[1][spec]=0
				
		max_spec=0
		index_of_max_spec=0
		if(above_sea>below_sea):
			for spec in range(len(movie_genre_distance[0])):
				if (movie_genre_distance[1][spec]>max_spec) and (movie_genre_distance[0][spec] in above_sea_level_movie_gendres):
					max_spec=movie_genre_distance[1][spec]
					index_of_max_spec=spec
		elif(above_sea<below_sea):
			for spec in range(len(movie_genre_distance[0])):
				if (movie_genre_distance[1][spec]>max_spec) and (movie_genre_distance[0][spec] in below_sea_level_movie_gendres):
					max_spec=movie_genre_distance[1][spec]
					index_of_max_spec=spec		
		elif(above_sea==below_sea):
			for spec in range(len(movie_genre_distance[0])):
				max_spec=movie_genre_distance[1][spec]
				index_of_max_spec=spec
					
		preferable_genres.append(movie_genre_distance[0][index_of_max_spec])
	
	preferable_genre_counter={}
	for x in preferable_genres:
		try:
			preferable_genre_counter[x]=preferable_genre_counter[x]+1
		except:
			preferable_genre_counter[x]=1
	if(len(preferable_genre_counter)==0):
		return-1	
	preferable_genre=''
	max_pref=0
	for item in preferable_genre_counter:
		if(preferable_genre_counter[item]>max_pref):
			max_pref=preferable_genre_counter[item]
			preferable_genre=item
	return genre_list.index(preferable_genre)	


def experience_factor(user_id):
	# execute our Query
	statement="select count(*)"
	statement=statement+" from movie_info inner join ratings on (item_id=movie_id)"
	statement=statement+" where ratings.user_id='"+user_id+"'"	
	cursor.execute(statement)
	# retrieve the records from the database
	db_recs = cursor.fetchall()
	movies_rated=db_recs[0][0]
	
	if(movies_rated<=100):
		return 0
	elif(movies_rated<=200):
		return 1
	elif(movies_rated<=300):
		return 2
	elif(movies_rated<=400):
		return 3
	elif(movies_rated<=500):
		return 4
	elif(movies_rated<=600):
		return 5
	else:
		return 6
	

def sea_level_factor(user_id):
	# execute our Query
	statement="select *"
	statement=statement+" from movie_info inner join ratings on (item_id=movie_id)"
	statement=statement+" where ratings.user_id='"+user_id+"'"	
	cursor.execute(statement)
	# retrieve the records from the database
	preferable_genres=[]
	db_recs = cursor.fetchall()
	total_above_the_sea=0
	total_below_the_sea=0
	for x in db_recs:
		truth_gendre_dict={}		
		truth_gendre_dict['is_unknown']=x[5]
		truth_gendre_dict['is_action']=x[6]
		truth_gendre_dict['is_adventure']=x[7]
		truth_gendre_dict['is_animation']=x[8]
		truth_gendre_dict['is_childrens']=x[9]
		truth_gendre_dict['is_comedy']=x[10]
		truth_gendre_dict['is_crime']=x[11]
		truth_gendre_dict['is_documentary']=x[12]
		truth_gendre_dict['is_drama']=x[13]
		truth_gendre_dict['is_fantasy']=x[14]
		truth_gendre_dict['is_film_noir']=x[15]
		truth_gendre_dict['is_horror']=x[16]
		truth_gendre_dict['is_musical']=x[17]
		truth_gendre_dict['is_mystery']=x[18]
		truth_gendre_dict['is_romance']=x[19]
		truth_gendre_dict['is_sci_fi']=x[20]
		truth_gendre_dict['is_thriller']=x[21]
		truth_gendre_dict['is_war']=x[22]
		truth_gendre_dict['is_western']=x[23]
		movie_genre_distance=[]
		for item in truth_gendre_dict:
			if (truth_gendre_dict[item]==True):
				movie_genre_distance.append(item)
		above_sea=0
		below_sea=0
		for spec in range(len(movie_genre_distance)):
			if movie_genre_distance[spec] in above_sea_level_movie_gendres:
				above_sea=above_sea+1
			elif movie_genre_distance[spec] in below_sea_level_movie_gendres:
				below_sea=below_sea+1
		if(above_sea>below_sea):
			total_above_the_sea=total_above_the_sea+1
		elif(above_sea<below_sea):
			total_below_the_sea=total_below_the_sea+1

	if(total_above_the_sea>total_below_the_sea):
		return 1
	elif (total_above_the_sea<total_below_the_sea):
		return -1
	else:
		return 0
# execute our Query
statement="select * from user_info "
cursor.execute(statement)
# retrieve the records from the database
db_records = cursor.fetchall()
with open("user_vectors.txt","w") as user_file:
	for x in db_records:
		entry=()
		user_id=x[0]
		age=x[1]		
		gendre=x[2]
		occ=x[3]
		zip_code=x[4]
		
		#age_gender index
		if(age>=7 and age<=14 and gendre=='M'):
			entry=entry+('0',)
		elif(age>=15 and age<=19 and gendre=='M'):
			entry=entry+('1',)
		elif(age>=20 and age<=29 and gendre=='M'):
			entry=entry+('2',)
		elif(age>=30 and age<=49 and gendre=='M'):
			entry=entry+('3',)
		elif(age>=50 and age<=64 and gendre=='M'):
			entry=entry+('4',)
		elif(age>=65 and age<=73 and gendre=='M'):
			entry=entry+('5',)
		elif(age>=7 and age<=14 and gendre=='F'):
			entry=entry+('6',)
		elif(age>=15 and age<=19 and gendre=='F'):
			entry=entry+('7',)
		elif(age>=20 and age<=29 and gendre=='F'):
			entry=entry+('8',)
		elif(age>=30 and age<=49 and gendre=='F'):
			entry=entry+('9',)
		elif(age>=50 and age<=64 and gendre=='F'):
			entry=entry+('10',)
		elif(age>=65 and age<=73 and gendre=='F'):
			entry=entry+('11',)
		
		#occupation index
		if(occ=='doctor' or occ=='healthcare'):
			entry=entry+('0',)
		elif(occ=='artist' or occ=='entertainment'):
			entry=entry+('1',)
		elif(occ=='administrator' or occ=='executive'):
			entry=entry+('2',)
		elif(occ=='scientist' or occ=='engineer'):
			entry=entry+('3',)
		elif(occ=='none' or occ=='other'):
			entry=entry+('4',)
		elif(occ=='educator' or occ=='writer' or occ=='librarian'):
			entry=entry+('5',)
		elif(occ=='programmer' or occ=='technician'):
			entry=entry+('6',)
		elif(occ=='salesman' or occ=='marketing'):
			entry=entry+('7',)
		elif(occ=='student'):
			entry=entry+('8',)
		elif(occ=='lawyer'):
			entry=entry+('9',)
		elif(occ=='retired'):
			entry=entry+('10',)
		elif(occ=='homemaker'):
			entry=entry+('11',)
			#location index
		if(zip_code[0]=='0'):
			entry=entry+('0',)
		elif(zip_code[0]=='1'):
			entry=entry+('1',)
		elif(zip_code[0]=='2'):
			entry=entry+('2',)
		elif(zip_code[0]=='3'):
			entry=entry+('3',)
		elif(zip_code[0]=='4'):
			entry=entry+('4',)
		elif(zip_code[0]=='5'):
			entry=entry+('5',)
		elif(zip_code[0]=='6'):
			entry=entry+('6',)
		elif(zip_code[0]=='7'):
			entry=entry+('7',)
		elif(zip_code[0]=='8'):
			entry=entry+('8',)
		else:
			entry=entry+('9',)
		
		experience_factor(user_id)	
		#good rating index
		entry=entry+(extract_genre(user_id,"and rating>=4"),)
		#medium rating index
		entry=entry+(extract_genre(user_id,"and rating=3"),)
		#bad rating index
		entry=entry+(extract_genre(user_id,"and rating<=2"),)
		#complete index
		entry=entry+(extract_genre(user_id,""),)
		#above_or_below
		entry=entry+(sea_level_factor(user_id),)	
		#experience indicator
		entry=entry+(experience_factor(user_id),)
				
		user_file.write(user_id+'|')
		user_file.write(str(int(entry[0]))+'|')
		user_file.write(str(int(entry[1]))+'|')
		user_file.write(str(int(entry[2]))+'|')
		user_file.write(str(int(entry[3]))+'|')
		user_file.write(str(int(entry[4]))+'|')
		user_file.write(str(int(entry[5]))+'|')
		user_file.write(str(int(entry[6]))+'|')
		user_file.write(str(int(entry[7]))+'|')
		user_file.write(str(int(entry[8]))+'\n')

