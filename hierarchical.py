#!/usr/bin/python
import math

def extract_fields(line):
	user_id=''
	age_gen=''
	occup=''
	zip_code=''
	prefer_movies=''
	average_movies=''
	bad_movies=''
	summary_movie=''
	above_below=''
	experience=''
	index=0
	while(line[index]!='|'):
		user_id=user_id+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		age_gen=age_gen+line[index]
		index=index+1
	index=index+1
	
	while(line[index]!='|'):
		occup=occup+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		zip_code=zip_code+line[index]
		index=index+1
	index=index+1
	
	
	while(line[index]!='|'):
		prefer_movies=prefer_movies+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		average_movies=average_movies+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		bad_movies=bad_movies+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		summary_movie=summary_movie+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		above_below=above_below+line[index]
		index=index+1
	index=index+1


	while(line[index]!='\n'):
		experience=experience+line[index]
		index=index+1
	
	return user_id,age_gen,occup,zip_code,prefer_movies,average_movies,bad_movies,summary_movie,above_below,experience
def extract_parametres(data):
	parametre_x=''
	parametre_y=''
	index=0
	while(data[index]!=','):
		parametre_x=parametre_x+data[index]
		index=index+1
	parametre_y=data[index+1:]
	return int(parametre_x),int(parametre_y) 
points=[]
point_labels=[]
with open('Euclidean_parametres.txt') as user_file,open('hierarchical_results.txt',"w") as results:
	for user_data in user_file: 	
		user_id,age_gen,occup,zip_code,prefer_movies,average_movies,bad_movies,summary_movie,above_below,experience=extract_fields(user_data)
				
		age_gendre_x,age_gendre_y=extract_parametres(age_gen)
		occupation_x,occupation_y=extract_parametres(occup)
		preferance_x,preferance_y=extract_parametres(prefer_movies)
		medium_x,medium_y=extract_parametres(average_movies)
		negative_x,negative_y=extract_parametres(bad_movies)
		overall_x,overall_y=extract_parametres(summary_movie)
		
		user_tup=(age_gendre_x,age_gendre_y,occupation_x,occupation_y,zip_code,preferance_x,preferance_y,medium_x,medium_y)
		user_tup=user_tup+(negative_x,negative_y,overall_x,overall_y,above_below,experience)
		points.append(user_tup)
		
		point_labels.append(user_id)
	
	while(len(points)>=58):
		min_distance=2**40
		point_a_of_min_distance=''
		point_b_of_min_distance=''
		for x in range(len(points)):
			for y in range(len(points)):
				if(x>y):
					#calculate euclidean distance between points[x],points[y]
					point_a_age_gen_x=points[x][0]
					point_b_age_gen_x=points[y][0]
					point_a_age_gen_y=points[x][1]
					point_b_age_gen_y=points[y][1]
					
					point_a_occ_x=points[x][2]
					point_b_occ_x=points[y][2]
					point_a_occ_y=points[x][3]
					point_b_occ_y=points[y][3]
					
					point_a_zip_x=points[x][4]
					point_b_zip_x=points[y][4]

					point_a_pref_x=points[x][5]
					point_b_pref_x=points[y][5]
					point_a_pref_y=points[x][6]
					point_b_pref_y=points[y][6]

					point_a_av_x=points[x][7]
					point_b_av_x=points[y][7]
					point_a_av_y=points[x][8]
					point_b_av_y=points[y][8]

					point_a_non_pref_x=points[x][9]
					point_b_non_pref_x=points[y][9]
					point_a_non_pref_y=points[x][10]
					point_b_non_pref_y=points[y][10]

					point_a_overall_x=points[x][11]
					point_b_overall_x=points[y][11]
					point_a_overall_y=points[x][12]
					point_b_overall_y=points[y][12]

					point_a_above_below_x=points[x][13]
					point_b_above_below_x=points[y][13]

					point_a_exp_x=points[x][14]
					point_b_exp_x=points[y][14]
						
					first_sub_route_x=int(point_b_age_gen_x)-int(point_a_age_gen_x)
					first_sub_route_x=first_sub_route_x**2
					first_sub_route_y=int(point_b_age_gen_y)-int(point_a_age_gen_y)
					first_sub_route_y=first_sub_route_y**2
					first_sub_route=0.12*(first_sub_route_x+first_sub_route_y)

					sec_sub_route_x=int(point_b_occ_x)-int(point_a_occ_x)
					sec_sub_route_x=sec_sub_route_x**2
					sec_sub_route_y=int(point_b_occ_y)-int(point_a_occ_y)
					sec_sub_route_y=sec_sub_route_y**2
					sec_sub_route=0.10*(sec_sub_route_x+sec_sub_route_y)

					third_sub_route_x=int(point_b_zip_x)-int(point_a_zip_x)
					third_sub_route_x=third_sub_route_x**2
					third_sub_route=0.02*(third_sub_route_x)	

					fourth_sub_route_x=int(point_b_pref_x)-int(point_a_pref_x)
					fourth_sub_route_x=fourth_sub_route_x**2
					fourth_sub_route_y=int(point_b_pref_y)-int(point_a_pref_y)
					fourth_sub_route_y=fourth_sub_route_y**2
					fourth_sub_route=0.15*(fourth_sub_route_x+fourth_sub_route_y)

					fifth_sub_route_x=int(point_b_av_x)-int(point_a_av_x)
					fifth_sub_route_x=fifth_sub_route_x**2
					fifth_sub_route_y=int(point_b_av_y)-int(point_a_av_y)
					fifth_sub_route_y=fifth_sub_route_y**2
					fifth_sub_route=0.15*(fifth_sub_route_x+fifth_sub_route_y)

					sixth_sub_route_x=int(point_b_non_pref_x)-int(point_a_non_pref_x)
					sixth_sub_route_x=sixth_sub_route_x**2
					sixth_sub_route_y=int(point_b_non_pref_y)-int(point_a_non_pref_y)
					sixth_sub_route_y=sixth_sub_route_y**2
					sixth_sub_route=0.15*(sixth_sub_route_x+sixth_sub_route_y)

					seventh_sub_route_x=int(point_b_overall_x)-int(point_a_overall_x)
					seventh_sub_route_x=seventh_sub_route_x**2
					seventh_sub_route_y=int(point_b_overall_y)-int(point_a_overall_y)
					seventh_sub_route_y=seventh_sub_route_y**2
					seventh_sub_route=0.15*(seventh_sub_route_x+seventh_sub_route_y)

					eighth_sub_route_x=int(point_b_above_below_x)-int(point_a_above_below_x)
					eighth_sub_route_x=eighth_sub_route_x**2
					eighth_sub_route=0.08*(eighth_sub_route_x)

					ninth_sub_route_x=int(point_b_exp_x)-int(point_a_exp_x)
					ninth_sub_route_x=ninth_sub_route_x**2
					ninth_sub_route=0.08*(ninth_sub_route_x)

					complete_sub_route=first_sub_route+sec_sub_route+third_sub_route+fourth_sub_route+fifth_sub_route+sixth_sub_route
					complete_sub_route=complete_sub_route+seventh_sub_route+eighth_sub_route+ninth_sub_route_x

					distance_b_from_a=math.sqrt(complete_sub_route)

					if(distance_b_from_a<min_distance):
						min_distance=distance_b_from_a
						point_a_of_min_distance=points[x]
						point_b_of_min_distance=points[y]
						index_of_point_b=y
						index_of_point_a=x
		
		print("Minimum Distance:{0}".format(min_distance))
		results.write("Minimum Distance:{0}\n".format(min_distance))
		value_of_point_b=points[index_of_point_b]
		value_of_point_a=points[index_of_point_a]
		label_of_point_b=point_labels[index_of_point_b]
		label_of_point_a=point_labels[index_of_point_a]
		print("Value of point b:{0} Index of point b:{1} Label of point b:{2}".format(value_of_point_b,index_of_point_b,label_of_point_b))
		results.write("Value of point b:{0} Index of point b:{1} Label of point b:{2}\n".format(value_of_point_b,index_of_point_b,label_of_point_b))
		print("Value of point a:{0} Index of point a:{1} Label of point a:{2}".format(value_of_point_a,index_of_point_a,label_of_point_a))
		results.write("Value of point a:{0} Index of point a:{1} Label of point a:{2}\n".format(value_of_point_a,index_of_point_a,label_of_point_a))
		
		del points[max(index_of_point_b,index_of_point_a)]
		del points[min(index_of_point_b,index_of_point_a)]

		del point_labels[max(index_of_point_b,index_of_point_a)]
		del point_labels[min(index_of_point_b,index_of_point_a)]

		average_of_cluster_age_gen_x=(value_of_point_b[0]+value_of_point_a[0])/2
		average_of_cluster_age_gen_y=(value_of_point_b[1]+value_of_point_a[1])/2
		
		average_of_cluster_occ_x=(value_of_point_b[2]+value_of_point_a[2])/2
		average_of_cluster_occ_y=(value_of_point_b[3]+value_of_point_a[3])/2

		average_of_cluster_zip_x=(int(value_of_point_b[4])+int(value_of_point_a[4]))/2

		average_of_cluster_pref_x=(value_of_point_b[5]+value_of_point_a[5])/2
		average_of_cluster_pref_y=(value_of_point_b[6]+value_of_point_a[6])/2

		average_of_cluster_av_x=(value_of_point_b[7]+value_of_point_a[7])/2
		average_of_cluster_av_y=(value_of_point_b[8]+value_of_point_a[8])/2

		average_of_cluster_non_pref_x=(value_of_point_b[9]+value_of_point_a[9])/2	
		average_of_cluster_non_pref_y=(value_of_point_b[10]+value_of_point_a[10])/2

		average_of_cluster_overall_x=(value_of_point_b[11]+value_of_point_a[11])/2
		average_of_cluster_overall_y=(value_of_point_b[12]+value_of_point_a[12])/2

		average_of_cluster_above_below_x=(int(value_of_point_b[13])+int(value_of_point_a[13]))/2
			
		average_of_cluster_exp_x=(int(value_of_point_b[14])+int(value_of_point_a[14]))/2



		average=(average_of_cluster_age_gen_x,average_of_cluster_age_gen_y)
		average=average+(average_of_cluster_occ_x,average_of_cluster_occ_y)
		average=average+(average_of_cluster_zip_x,average_of_cluster_pref_x)
		average=average+(average_of_cluster_pref_y,average_of_cluster_av_x)
		average=average+(average_of_cluster_av_y,average_of_cluster_non_pref_x)
		average=average+(average_of_cluster_non_pref_y,average_of_cluster_overall_x)
		average=average+(average_of_cluster_overall_y,average_of_cluster_above_below_x)
		average=average+(average_of_cluster_exp_x,)
		
		print("Centroid of new cluster:{0}".format(average))
		results.write("Centroid of new cluster:{0}\n".format(average))
		points.append(average)
		point_labels.append(label_of_point_b+'/'+label_of_point_a)
		print('-----------------------------------------------------------------------------------')
		results.write('-----------------------------------------------------------------------------------\n')	
				
					
