#!/usr/bin/python
import random
import math


def extract_fields(line):
	user_id=''
	age_gen=''
	occup=''
	zip_code=''
	prefer_movies=''
	average_movies=''
	bad_movies=''
	summary_movie=''
	above_below=''
	experience=''
	index=0
	while(line[index]!='|'):
		user_id=user_id+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		age_gen=age_gen+line[index]
		index=index+1
	index=index+1
	
	while(line[index]!='|'):
		occup=occup+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		zip_code=zip_code+line[index]
		index=index+1
	index=index+1
	
	
	while(line[index]!='|'):
		prefer_movies=prefer_movies+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		average_movies=average_movies+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		bad_movies=bad_movies+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		summary_movie=summary_movie+line[index]
		index=index+1
	index=index+1

	while(line[index]!='|'):
		above_below=above_below+line[index]
		index=index+1
	index=index+1


	while(line[index]!='\n'):
		experience=experience+line[index]
		index=index+1
	
	return user_id,age_gen,occup,zip_code,prefer_movies,average_movies,bad_movies,summary_movie,above_below,experience
def extract_parametres(data):
	parametre_x=''
	parametre_y=''
	index=0
	while(data[index]!=','):
		parametre_x=parametre_x+data[index]
		index=index+1
	parametre_y=data[index+1:]
	return parametre_x,parametre_y 

groups={}
for x in range(58):
	cont=True	
	while(cont):	
		centroid=("0",)
		age_gendre_x=random.choice([-1,1])
		age_gendre_y=random.choice([1,2,3,4,5,6])
		centroid=centroid+(age_gendre_x,age_gendre_y,)
		
		occ_x,occ_y=random.choice([(1,4),(-3,-4),(-3,3),(2,3),(0,0),(2,-2),(3,1),(-2,1),(0,1),(1,1),(-2,-2),(-3,-3)])
		centroid=centroid+(occ_x,occ_y)
		
		zip_code=random.choice([0,1,2,3,4,5,6,7,8,9])
		centroid=centroid+(zip_code,)		
		
		film_x,film_y=random.choice([(-9,9),(-8,8),(-7,7),(-6,6),(-5,5),(-4,4),(-3,3),(-2,2),(-1,1),(0,0),(1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9)])
		centroid=centroid+(film_x,film_y)
		
		film_x,film_y=random.choice([(-9,9),(-8,8),(-7,7),(-6,6),(-5,5),(-4,4),(-3,3),(-2,2),(-1,1),(0,0),(1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9)])
		centroid=centroid+(film_x,film_y)
		
		film_x,film_y=random.choice([(-9,9),(-8,8),(-7,7),(-6,6),(-5,5),(-4,4),(-3,3),(-2,2),(-1,1),(0,0),(1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9)])
		centroid=centroid+(film_x,film_y)
		
		film_x,film_y=random.choice([(-9,9),(-8,8),(-7,7),(-6,6),(-5,5),(-4,4),(-3,3),(-2,2),(-1,1),(0,0),(1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9)])
		centroid=centroid+(film_x,film_y)
		
		above_below=random.choice([-1,1])
		centroid=centroid+(above_below,)
		
		exp=random.choice([0,1,2,3,4,5])
		centroid=centroid+(exp,)
		if(centroid in groups):
			cont=True
		else:
			groups[centroid]=[]
			cont=False

print(len(groups))

#-----------------------------------------------------------------------------------------------
#main algorithm starts here
user_counter=0						
era_counter=1	
era_index=0
stop_generation=False
while(stop_generation==False):
	with open("Euclidean_parametres.txt") as users:
		for lines in users:
			user_id,age_gen,occup,zip_code,prefer_movies,average_movies,bad_movies,summary_movie,above_below,experience=extract_fields(lines)
			
			age_gendre_x,age_gendre_y=extract_parametres(age_gen)
			occupation_x,occupation_y=extract_parametres(occup)
			preferance_x,preferance_y=extract_parametres(prefer_movies)
			medium_x,medium_y=extract_parametres(average_movies)
			negative_x,negative_y=extract_parametres(bad_movies)
			overall_x,overall_y=extract_parametres(summary_movie)
			
			user_id=int(user_id)
			age_gender_x=int(age_gendre_x)
			age_gender_y=int(age_gendre_y)
			occupation_x=int(occupation_x)
			occupation_y=int(occupation_y)
			zip_code=int(zip_code)
			preferance_x=int(preferance_x)
			preferance_y=int(preferance_y)
			medium_x=int(medium_x)
			medium_y=int(medium_y)
			negative_x=int(negative_x)
			negative_y=int(negative_y)
			overall_x=int(overall_x)
			overall_y=int(overall_y)
			above_below=int(above_below)
			experience=int(experience)
			
			user_vector=(user_id,age_gender_x,age_gender_y,occupation_x,occupation_y,zip_code,preferance_x,preferance_y,medium_x,medium_y)	
			user_vector=user_vector+(negative_x,negative_y,overall_x,overall_y,above_below,experience)

			min_distance=2**40
			min_centroid=()	
			for centroids in sorted(groups):
				#calculate euclidean distance for every group		
				age_gender_x_par=age_gender_x-centroids[1]
				age_gender_x_par=age_gender_x_par**2
				age_gender_y_par=age_gender_y-centroids[2]
				age_gender_y_par=age_gender_y_par**2
				age_gender_sub_rout=age_gender_x_par+age_gender_y_par
				age_gender_sub_rout=0.12*age_gender_sub_rout

				#------------------------------------------
				occupation_x_par=occupation_x-centroids[3]
				occupation_x_par=occupation_x_par**2
				occupation_y_par=occupation_y-centroids[4]
				occupation_y_par=occupation_y_par**2
				occupation_sub_rout=occupation_x_par+occupation_y_par
				occupation_sub_rout=0.10*occupation_sub_rout

				#------------------------------------------
				zip_code_par=zip_code-centroids[5]
				zip_code_par=zip_code_par**2
				zip_code_sub_rout=0.02*zip_code_par
				
				#------------------------------------------
				preferance_x_par=preferance_x-centroids[6]
				preferance_x_par=preferance_x_par**2
				preferance_y_par=preferance_y-centroids[7]
				preferance_y_par=preferance_y_par**2
				preferance_sub_rout=preferance_x_par+preferance_y_par
				preferance_sub_rout=0.15*preferance_sub_rout

				#------------------------------------------
				medium_x_par=medium_x-centroids[8]
				medium_x_par=medium_x_par**2
				medium_y_par=medium_y-centroids[9]
				medium_y_par=medium_y_par**2
				medium_sub_rout=medium_x_par+medium_y_par
				medium_sub_rout=0.15*medium_sub_rout

				#------------------------------------------
				negative_x_par=negative_x-centroids[10]
				negative_x_par=negative_x_par**2
				negative_y_par=negative_y-centroids[11]
				negative_y_par=negative_y_par**2
				negative_sub_rout=negative_x_par+negative_y_par
				negative_sub_rout=0.15*negative_sub_rout

				#------------------------------------------
				overall_x_par=overall_x-centroids[12]
				overall_x_par=overall_x_par**2
				overall_y_par=overall_y-centroids[13]
				overall_y_par=overall_y_par**2
				overall_sub_rout=overall_x_par+overall_y_par
				overall_sub_rout=0.15*overall_sub_rout

				#-----------------------------------------
				above_below_par=above_below-centroids[14]
				above_below_par=above_below_par**2
				above_below_sub_rout=0.08*above_below_par

				#-----------------------------------------
				experience_par=experience-centroids[15]
				experience_par=experience_par**2
				experience_sub_rout=0.08*experience_par
				
				#euclidean distance
				formula_subroute=age_gender_sub_rout+occupation_sub_rout+zip_code_sub_rout+preferance_sub_rout+medium_sub_rout
				formula_subroute=formula_subroute+negative_sub_rout+overall_sub_rout+above_below_sub_rout+experience_sub_rout
				d_user_vector_from_centroid=math.sqrt(formula_subroute)
				if(d_user_vector_from_centroid<min_distance):
					min_distance=d_user_vector_from_centroid
					min_centroid=centroids		
			groups[min_centroid].append(user_vector)	
		#end of reading all users 
		era_index=era_index+1
		print("End of era:{0}".format(era_index))
  		#------------------------------------------------------------------------------------------------------------- 
				
		groups_replica={}	
		for centroids in groups:		
			average_centroid=("0",)
			count_age_gender_x=0
			count_age_gender_y=0
			count_occupation_x=0
			count_occupation_y=0
			count_zip_code=0
			count_preferance_x=0
			count_preferance_y=0
			count_medium_x=0
			count_medium_y=0
			count_negative_x=0
			count_negative_y=0
			count_overall_x=0
			count_overall_y=0
			count_above_below=0
			count_experience=0
			total_counter=0
			for vectors in groups[centroids]:
				total_counter=total_counter+1
				count_age_gender_x=count_age_gender_x+vectors[1]
				count_age_gender_y=count_age_gender_y+vectors[2]
				count_occupation_x=count_occupation_x+vectors[3]
				count_occupation_y=count_occupation_y+vectors[4]
				count_zip_code=count_zip_code+vectors[5]
				count_preferance_x=count_preferance_x+vectors[6]
				count_preferance_y=count_preferance_y+vectors[7]
				count_medium_x=count_medium_x+vectors[8]
				count_medium_y=count_medium_y+vectors[9]
				count_negative_x=count_negative_x+vectors[10]
				count_negative_y=count_negative_y+vectors[11]
				count_overall_x=count_overall_x+vectors[12]
				count_overall_y=count_overall_y+vectors[13]
				count_above_below=count_above_below+vectors[14]
				count_experience=count_experience+vectors[15]
			try:			
				average_age_gender_x=math.floor(count_age_gender_x/total_counter)
				average_age_gender_y=math.floor(count_age_gender_y/total_counter)
				average_occupation_x=math.floor(count_occupation_x/total_counter)
				average_occupation_y=math.floor(count_occupation_y/total_counter)
				average_zip_code=math.floor(count_zip_code/total_counter)
				average_preferance_x=math.floor(count_preferance_x/total_counter)
				average_preferance_y=math.floor(count_preferance_y/total_counter)
				average_medium_x=math.floor(count_medium_x/total_counter)
				average_medium_y=math.floor(count_medium_y/total_counter)
				average_negative_x=math.floor(count_negative_x/total_counter)
				average_negative_y=math.floor(count_negative_y/total_counter)
				average_overall_x=math.floor(count_overall_x/total_counter)
				average_overall_y=math.floor(count_overall_y/total_counter)
				average_above_below=math.floor(count_above_below/total_counter)
				average_experience=math.floor(count_experience/total_counter)
				average_centroid=average_centroid+(average_age_gender_x,average_age_gender_y,average_occupation_x,average_occupation_y)		
				average_centroid=average_centroid+(average_zip_code,average_preferance_x,average_preferance_y,average_medium_x)				
				average_centroid=average_centroid+(average_medium_y,average_negative_x,average_negative_y,average_overall_x)				
				average_centroid=average_centroid+(average_overall_y,average_above_below,average_experience)
				if(average_centroid in groups_replica):
					print("BUSTED")
					break
				else:
					groups_replica[average_centroid]=[]
					for elements in groups[centroids]: 
						groups_replica[average_centroid].append(elements)
			except:
				if(centroids in groups_replica):
					print("BUSTED")
					break
				else:
					groups_replica[centroids]=[]
					for elements in groups[centroids]: 
						groups_replica[centroids].append(elements)
		
		#user_counter=0		
		#for cents in groups:
		#	print(len(groups[cents]))
		#	user_counter=user_counter+len(groups[cents])
		#print(user_counter)
		print("-----------------------------------")
		user_counter=0		
		for cents in groups_replica:
			print(len(groups_replica[cents]))
			user_counter=user_counter+len(groups_replica[cents])
		print(user_counter)
		print("-----------------------------------")	 
		if(groups_replica!=groups):			
			era_counter=0			
			groups={}
			for cents in groups_replica:
				groups[cents]=[]
			groups_replica={}
		else:
			era_counter=era_counter+1
			if(era_counter>2):
				stop_generation=True
			groups={}
			for cents in groups_replica:
				groups[cents]=[]
			groups_replica={}

#----------------------------------------------------------------------------------------
with open("Euclidean_parametres.txt") as users,open("Euclidean_results.txt","w") as results:
	for lines in users:
		user_id,age_gen,occup,zip_code,prefer_movies,average_movies,bad_movies,summary_movie,above_below,experience=extract_fields(lines)
			
		age_gendre_x,age_gendre_y=extract_parametres(age_gen)
		occupation_x,occupation_y=extract_parametres(occup)
		preferance_x,preferance_y=extract_parametres(prefer_movies)
		medium_x,medium_y=extract_parametres(average_movies)
		negative_x,negative_y=extract_parametres(bad_movies)
		overall_x,overall_y=extract_parametres(summary_movie)
		
		user_id=int(user_id)
		age_gender_x=int(age_gendre_x)
		age_gender_y=int(age_gendre_y)
		occupation_x=int(occupation_x)
		occupation_y=int(occupation_y)
		zip_code=int(zip_code)
		preferance_x=int(preferance_x)
		preferance_y=int(preferance_y)
		medium_x=int(medium_x)
		medium_y=int(medium_y)
		negative_x=int(negative_x)
		negative_y=int(negative_y)
		overall_x=int(overall_x)
		overall_y=int(overall_y)
		above_below=int(above_below)
		experience=int(experience)
		user_vector=(user_id,age_gender_x,age_gender_y,occupation_x,occupation_y,zip_code,preferance_x,preferance_y,medium_x,medium_y)	
		user_vector=user_vector+(negative_x,negative_y,overall_x,overall_y,above_below,experience)

		min_distance=2**40
		min_centroid=()	
		for centroids in sorted(groups):
			#calculate euclidean distance for every group		
			age_gender_x_par=age_gender_x-centroids[1]
			age_gender_x_par=age_gender_x_par**2
			age_gender_y_par=age_gender_y-centroids[2]
			age_gender_y_par=age_gender_y_par**2
			age_gender_sub_rout=age_gender_x_par+age_gender_y_par
			age_gender_sub_rout=0.12*age_gender_sub_rout
				#------------------------------------------
			occupation_x_par=occupation_x-centroids[3]
			occupation_x_par=occupation_x_par**2
			occupation_y_par=occupation_y-centroids[4]
			occupation_y_par=occupation_y_par**2
			occupation_sub_rout=occupation_x_par+occupation_y_par
			occupation_sub_rout=0.10*occupation_sub_rout
				#------------------------------------------
			zip_code_par=zip_code-centroids[5]
			zip_code_par=zip_code_par**2
			zip_code_sub_rout=0.02*zip_code_par
			
			#------------------------------------------
			preferance_x_par=preferance_x-centroids[6]
			preferance_x_par=preferance_x_par**2
			preferance_y_par=preferance_y-centroids[7]
			preferance_y_par=preferance_y_par**2
			preferance_sub_rout=preferance_x_par+preferance_y_par
			preferance_sub_rout=0.15*preferance_sub_rout
				#------------------------------------------
			medium_x_par=medium_x-centroids[8]
			medium_x_par=medium_x_par**2
			medium_y_par=medium_y-centroids[9]
			medium_y_par=medium_y_par**2
			medium_sub_rout=medium_x_par+medium_y_par
			medium_sub_rout=0.15*medium_sub_rout
				#------------------------------------------
			negative_x_par=negative_x-centroids[10]
			negative_x_par=negative_x_par**2
			negative_y_par=negative_y-centroids[11]
			negative_y_par=negative_y_par**2
			negative_sub_rout=negative_x_par+negative_y_par
			negative_sub_rout=0.15*negative_sub_rout
				#------------------------------------------
			overall_x_par=overall_x-centroids[12]
			overall_x_par=overall_x_par**2
			overall_y_par=overall_y-centroids[13]
			overall_y_par=overall_y_par**2
			overall_sub_rout=overall_x_par+overall_y_par
			overall_sub_rout=0.15*overall_sub_rout
				#-----------------------------------------
			above_below_par=above_below-centroids[14]
			above_below_par=above_below_par**2
			above_below_sub_rout=0.08*above_below_par
				#-----------------------------------------
			experience_par=experience-centroids[15]
			experience_par=experience_par**2
			experience_sub_rout=0.08*experience_par
			
			#euclidean distance
			formula_subroute=age_gender_sub_rout+occupation_sub_rout+zip_code_sub_rout+preferance_sub_rout+medium_sub_rout
			formula_subroute=formula_subroute+negative_sub_rout+overall_sub_rout+above_below_sub_rout+experience_sub_rout
			d_user_vector_from_centroid=math.sqrt(formula_subroute)
			if(d_user_vector_from_centroid<min_distance):
				min_distance=d_user_vector_from_centroid
				min_centroid=centroids		
		groups[min_centroid].append(user_vector)	
	#end of reading all users 
	for centroids in groups:
		results.write("\nCentroid:{0}\n".format(centroids))
		readable_result="centroid\n"
		if(centroids[1]==-1)and(centroids[2]==1):
			readable_result=readable_result+"7-14M\n"
		elif(centroids[1]==-1)and(centroids[2]==2):
			readable_result=readable_result+"15-19M\n"
		elif(centroids[1]==-1)and(centroids[2]==3):
			readable_result=readable_result+"20-29M\n"
		elif(centroids[1]==-1)and(centroids[2]==4):
			readable_result=readable_result+"30-49M\n"
		elif(centroids[1]==-1)and(centroids[2]==5):
			readable_result=readable_result+"50-64M\n"
		elif(centroids[1]==-1)and(centroids[2]==6):
			readable_result=readable_result+"65-73M\n"
		elif(centroids[1]==1)and(centroids[2]==1):
			readable_result=readable_result+"7-14F\n"
		elif(centroids[1]==1)and(centroids[2]==2):
			readable_result=readable_result+"15-19F\n"
		elif(centroids[1]==1)and(centroids[2]==3):
			readable_result=readable_result+"20-29F\n"
		elif(centroids[1]==1)and(centroids[2]==4):
			readable_result=readable_result+"30-49F\n"
		elif(centroids[1]==1)and(centroids[2]==5):
			readable_result=readable_result+"50-64F\n"
		elif(centroids[1]==1)and(centroids[2]==6):
			readable_result=readable_result+"65-73F\n"

		if(centroids[3]==1)and(centroids[4]==4):
			readable_result=readable_result+"Doctor-Healthcare\n"
		elif(centroids[3]==-3)and(centroids[4]==-4):
			readable_result=readable_result+"Artist-Entertainment\n"
		elif(centroids[3]==-3)and(centroids[4]==3):
			readable_result=readable_result+"Administrator-Executive\n"
		elif(centroids[3]==2)and(centroids[4]==3):
			readable_result=readable_result+"Scientist-Engineer\n"
		elif(centroids[3]==0)and(centroids[4]==0):
			readable_result=readable_result+"None-Other\n"
		elif(centroids[3]==2)and(centroids[4]==-2):
			readable_result=readable_result+"Educator-Writter-Librarian\n"
		elif(centroids[3]==3)and(centroids[4]==1):
			readable_result=readable_result+"Programmer-Technician\n"
		elif(centroids[3]==-2)and(centroids[4]==1):
			readable_result=readable_result+"Salesman-Marketing\n"
		elif(centroids[3]==0)and(centroids[4]==1):
			readable_result=readable_result+"Student\n"
		elif(centroids[3]==1)and(centroids[4]==-1):
			readable_result=readable_result+"Lawyer\n"
		elif(centroids[3]==-2)and(centroids[4]==-2):
			readable_result=readable_result+"Retired\n"
		elif(centroids[3]==-3)and(centroids[4]==-3):
			readable_result=readable_result+"Homemaker\n"
		
		readable_result=readable_result+("Zip starts with:{0}\n".format(centroids[5]))
		
		if(centroids[6]==0)and(centroids[7]==0):
			readable_result=readable_result+"Prefered Movie genre: Unknown\n"
		elif(centroids[6]==9)and(centroids[7]==9):
			readable_result=readable_result+"Prefered Movie genre: Horror\n"
		elif(centroids[6]==8)and(centroids[7]==8):
			readable_result=readable_result+"Prefered Movie genre: War\n"
		elif(centroids[6]==7)and(centroids[7]==7):
			readable_result=readable_result+"Prefered Movie genre: Drama\n"
		elif(centroids[6]==6)and(centroids[7]==6):
			readable_result=readable_result+"Prefered Movie genre: Crime\n"
		elif(centroids[6]==5)and(centroids[7]==5):
			readable_result=readable_result+"Prefered Movie genre: Mystery\n"
		elif(centroids[6]==4)and(centroids[7]==4):
			readable_result=readable_result+"Prefered Movie genre: Thriller\n"
		elif(centroids[6]==3)and(centroids[7]==3):
			readable_result=readable_result+"Prefered Movie genre: Adventure\n"
		elif(centroids[6]==2)and(centroids[7]==2):
			readable_result=readable_result+"Prefered Movie genre: Action\n"
		elif(centroids[6]==1)and(centroids[7]==1):
			readable_result=readable_result+"Prefered Movie genre: Fantasy\n"
		elif(centroids[6]==-1)and(centroids[7]==1):
			readable_result=readable_result+"Prefered Movie genre: Animation\n"
		elif(centroids[6]==-2)and(centroids[7]==2):
			readable_result=readable_result+"Prefered Movie genre: Documentary\n"
		elif(centroids[6]==-3)and(centroids[7]==3):
			readable_result=readable_result+"Prefered Movie genre: Sci-Fi\n"
		elif(centroids[6]==-4)and(centroids[7]==4):
			readable_result=readable_result+"Prefered Movie genre: Western\n"
		elif(centroids[6]==-5)and(centroids[7]==5):
			readable_result=readable_result+"Prefered Movie genre: Film-Noir\n"
		elif(centroids[6]==-6)and(centroids[7]==6):
			readable_result=readable_result+"Prefered Movie genre: Romance\n"
		elif(centroids[6]==7)and(centroids[7]==7):
			readable_result=readable_result+"Prefered Movie genre: Musical\n"
		elif(centroids[6]==-8)and(centroids[7]==8):
			readable_result=readable_result+"Prefered Movie genre: Comedy\n"
		elif(centroids[6]==-9)and(centroids[7]==9):
			readable_result=readable_result+"Prefered Movie genre: Children's\n"

		if(centroids[8]==0)and(centroids[9]==0):
			readable_result=readable_result+"Average Rating genre: Unknown\n"
		elif(centroids[8]==9)and(centroids[9]==9):
			readable_result=readable_result+"Average Rating genre: Horror\n"
		elif(centroids[8]==8)and(centroids[9]==8):
			readable_result=readable_result+"Average Rating genre: War\n"
		elif(centroids[8]==7)and(centroids[9]==7):
			readable_result=readable_result+"Average Rating genre: Drama\n"
		elif(centroids[8]==6)and(centroids[9]==6):
			readable_result=readable_result+"Average Rating genre: Crime\n"
		elif(centroids[8]==5)and(centroids[9]==5):
			readable_result=readable_result+"Average Rating genre: Mystery\n"
		elif(centroids[8]==4)and(centroids[9]==4):
			readable_result=readable_result+"Average Rating genre: Thriller\n"
		elif(centroids[8]==3)and(centroids[9]==3):
			readable_result=readable_result+"Average Rating genre: Adventure\n"
		elif(centroids[8]==2)and(centroids[9]==2):
			readable_result=readable_result+"Average Rating genre: Action\n"
		elif(centroids[8]==1)and(centroids[9]==1):
			readable_result=readable_result+"Average Rating genre: Fantasy\n"
		elif(centroids[8]==-1)and(centroids[9]==1):
			readable_result=readable_result+"Average Rating genre: Animation\n"
		elif(centroids[8]==-2)and(centroids[9]==2):
			readable_result=readable_result+"Average Rating genre: Documentary\n"
		elif(centroids[8]==-3)and(centroids[9]==3):
			readable_result=readable_result+"Average Rating genre: Sci-Fi\n"
		elif(centroids[8]==-4)and(centroids[9]==4):
			readable_result=readable_result+"Average Rating genre: Western\n"
		elif(centroids[8]==-5)and(centroids[9]==5):
			readable_result=readable_result+"Average Rating genre: Film-Noir\n"
		elif(centroids[8]==-6)and(centroids[9]==6):
			readable_result=readable_result+"Average Rating genre: Romance\n"
		elif(centroids[8]==7)and(centroids[9]==7):
			readable_result=readable_result+"Average Rating genre: Musical\n"
		elif(centroids[8]==-8)and(centroids[9]==8):
			readable_result=readable_result+"Average Rating genre: Comedy\n"
		elif(centroids[8]==-9)and(centroids[9]==9):
			readable_result=readable_result+"Average Rating genre: Children's\n"

		if(centroids[10]==0)and(centroids[11]==0):
			readable_result=readable_result+"Negative Rating genre: Unknown\n"
		elif(centroids[10]==9)and(centroids[11]==9):
			readable_result=readable_result+"Negative Rating genre: Horror\n"
		elif(centroids[10]==8)and(centroids[11]==8):
			readable_result=readable_result+"Negative Rating genre: War\n"
		elif(centroids[10]==7)and(centroids[11]==7):
			readable_result=readable_result+"Negative Rating genre: Drama\n"
		elif(centroids[10]==6)and(centroids[11]==6):
			readable_result=readable_result+"Negative Rating genre: Crime\n"
		elif(centroids[10]==5)and(centroids[11]==5):
			readable_result=readable_result+"Negative Rating genre: Mystery\n"
		elif(centroids[10]==4)and(centroids[11]==4):
			readable_result=readable_result+"Negative Rating genre: Thriller\n"
		elif(centroids[10]==3)and(centroids[11]==3):
			readable_result=readable_result+"Negative Rating genre: Adventure\n"
		elif(centroids[10]==2)and(centroids[11]==2):
			readable_result=readable_result+"Negative Rating genre: Action\n"
		elif(centroids[10]==1)and(centroids[11]==1):
			readable_result=readable_result+"Negative Rating genre: Fantasy\n"
		elif(centroids[10]==-1)and(centroids[11]==1):
			readable_result=readable_result+"Negative Rating genre: Animation\n"
		elif(centroids[10]==-2)and(centroids[11]==2):
			readable_result=readable_result+"Negative Rating genre: Documentary\n"
		elif(centroids[10]==-3)and(centroids[11]==3):
			readable_result=readable_result+"Negative Rating genre: Sci-Fi\n"
		elif(centroids[10]==-4)and(centroids[11]==4):
			readable_result=readable_result+"Negative Rating genre: Western\n"
		elif(centroids[10]==-5)and(centroids[11]==5):
			readable_result=readable_result+"Negative Rating genre: Film-Noir\n"
		elif(centroids[10]==-6)and(centroids[11]==6):
			readable_result=readable_result+"Negative Rating genre: Romance\n"
		elif(centroids[10]==7)and(centroids[11]==7):
			readable_result=readable_result+"Negative Rating genre: Musical\n"
		elif(centroids[10]==-8)and(centroids[11]==8):
			readable_result=readable_result+"Negative Rating genre: Comedy\n"
		elif(centroids[10]==-9)and(centroids[11]==9):
			readable_result=readable_result+"Negative Rating genre: Children's\n"


		if(centroids[12]==0)and(centroids[13]==0):
			readable_result=readable_result+"User's watching tendency for genre: Unknown\n"
		elif(centroids[12]==9)and(centroids[13]==9):
			readable_result=readable_result+"User's watching tendency for genre: Horror\n"
		elif(centroids[12]==8)and(centroids[13]==8):
			readable_result=readable_result+"User's watching tendency for genre: War\n"
		elif(centroids[12]==7)and(centroids[13]==7):
			readable_result=readable_result+"User's watching tendency for genre: Drama\n"
		elif(centroids[12]==6)and(centroids[13]==6):
			readable_result=readable_result+"User's watching tendency for genre: Crime\n"
		elif(centroids[12]==5)and(centroids[13]==5):
			readable_result=readable_result+"User's watching tendency for genre: Mystery\n"
		elif(centroids[12]==4)and(centroids[13]==4):
			readable_result=readable_result+"User's watching tendency for genre: Thriller\n"
		elif(centroids[12]==3)and(centroids[13]==3):
			readable_result=readable_result+"User's watching tendency for genre: Adventure\n"
		elif(centroids[12]==2)and(centroids[13]==2):
			readable_result=readable_result+"User's watching tendency for genre: Action\n"
		elif(centroids[12]==1)and(centroids[13]==1):
			readable_result=readable_result+"User's watching tendency for genre: Fantasy\n"
		elif(centroids[12]==-1)and(centroids[13]==1):
			readable_result=readable_result+"User's watching tendency for genre: Animation\n"
		elif(centroids[12]==-2)and(centroids[13]==2):
			readable_result=readable_result+"User's watching tendency for genre: Documentary\n"
		elif(centroids[12]==-3)and(centroids[13]==3):
			readable_result=readable_result+"User's watching tendency for genre: Sci-Fi\n"
		elif(centroids[12]==-4)and(centroids[13]==4):
			readable_result=readable_result+"User's watching tendency for genre: Western\n"
		elif(centroids[12]==-5)and(centroids[13]==5):
			readable_result=readable_result+"User's watching tendency for genre: Film-Noir\n"
		elif(centroids[12]==-6)and(centroids[13]==6):
			readable_result=readable_result+"User's watching tendency for genre: Romance\n"
		elif(centroids[12]==7)and(centroids[13]==7):
			readable_result=readable_result+"User's watching tendency for genre: Musical\n"
		elif(centroids[12]==-8)and(centroids[13]==8):
			readable_result=readable_result+"User's watching tendency for genre: Comedy\n"
		elif(centroids[12]==-9)and(centroids[13]==9):
			readable_result=readable_result+"User's watching tendency for genre: Children's\n"

		if(centroids[14]==-1):
			readable_result=readable_result+"User has a preference for movies that generate soft/positive emotions\n"
		elif(centroids[14]==1):
			readable_result=readable_result+"User has a preference for movies that generate strong and sometimes negative emotions\n"

		if(centroids[15]==0):
			readable_result=readable_result+"User is inexperienced judge of movies (movies_rated<=100)\n"
		elif(centroids[15]==1):
			readable_result=readable_result+"User has small experience in  judging movies (100<movies_rated<=200)\n"
		elif(centroids[15]==2):
			readable_result=readable_result+"User has moderate experience in  judging movies (200<movies_rated<=300)\n"
		elif(centroids[15]==3):
			readable_result=readable_result+"User has intermediate experience in  judging movies (300<movies_rated<=400)\n"
		elif(centroids[15]==4):
			readable_result=readable_result+"User has advanced experience in  judging movies (400<movies_rated<=500)\n"
		elif(centroids[15]==5):
			readable_result=readable_result+"User is a very experienced judge of movies (500<movies_rated<=600)\n"
		elif(centroids[15]==6):
			readable_result=readable_result+"User belongs in the most experienced gategory of judging movies (600<movies_rated\n"

		results.write("{0}\n".format(readable_result))
		for users in groups[centroids]:
			results.write(str(users)+'\n')
			readable_result=str(users[0])+"\n"
			if(users[1]==-1)and(users[2]==1):
				readable_result=readable_result+"7-14M\n"
			elif(users[1]==-1)and(users[2]==2):
				readable_result=readable_result+"15-19M\n"
			elif(users[1]==-1)and(users[2]==3):
				readable_result=readable_result+"20-29M\n"
			elif(users[1]==-1)and(users[2]==4):
				readable_result=readable_result+"30-49M\n"
			elif(users[1]==-1)and(users[2]==5):
				readable_result=readable_result+"50-64M\n"
			elif(users[1]==-1)and(users[2]==6):
				readable_result=readable_result+"65-73M\n"
			elif(users[1]==1)and(users[2]==1):
				readable_result=readable_result+"7-14F\n"
			elif(users[1]==1)and(users[2]==2):
				readable_result=readable_result+"15-19F\n"
			elif(users[1]==1)and(users[2]==3):
				readable_result=readable_result+"20-29F\n"
			elif(users[1]==1)and(users[2]==4):
				readable_result=readable_result+"30-49F\n"
			elif(users[1]==1)and(users[2]==5):
				readable_result=readable_result+"50-64F\n"
			elif(users[1]==1)and(users[2]==6):
				readable_result=readable_result+"65-73F\n"

			if(users[3]==1)and(users[4]==4):
				readable_result=readable_result+"Doctor-Healthcare\n"
			elif(users[3]==-3)and(users[4]==-4):
				readable_result=readable_result+"Artist-Entertainment\n"
			elif(users[3]==-3)and(users[4]==3):
				readable_result=readable_result+"Administrator-Executive\n"
			elif(users[3]==2)and(users[4]==3):
				readable_result=readable_result+"Scientist-Engineer\n"
			elif(users[3]==0)and(users[4]==0):
				readable_result=readable_result+"None-Other\n"
			elif(users[3]==2)and(users[4]==-2):
				readable_result=readable_result+"Educator-Writter-Librarian\n"
			elif(users[3]==3)and(users[4]==1):
				readable_result=readable_result+"Programmer-Technician\n"
			elif(users[3]==-2)and(users[4]==1):
				readable_result=readable_result+"Salesman-Marketing\n"
			elif(users[3]==0)and(users[4]==1):
				readable_result=readable_result+"Student\n"
			elif(users[3]==1)and(users[4]==-1):
				readable_result=readable_result+"Lawyer\n"
			elif(users[3]==-2)and(users[4]==-2):
				readable_result=readable_result+"Retired\n"
			elif(users[3]==-3)and(users[4]==-3):
				readable_result=readable_result+"Homemaker\n"
			
			readable_result=readable_result+("Zip starts with:{0}\n".format(users[5]))
			
			if(users[6]==0)and(users[7]==0):
				readable_result=readable_result+"Prefered Movie genre: Unknown\n"
			elif(users[6]==9)and(users[7]==9):
				readable_result=readable_result+"Prefered Movie genre: Horror\n"
			elif(users[6]==8)and(users[7]==8):
				readable_result=readable_result+"Prefered Movie genre: War\n"
			elif(users[6]==7)and(users[7]==7):
				readable_result=readable_result+"Prefered Movie genre: Drama\n"
			elif(users[6]==6)and(users[7]==6):
				readable_result=readable_result+"Prefered Movie genre: Crime\n"
			elif(users[6]==5)and(users[7]==5):
				readable_result=readable_result+"Prefered Movie genre: Mystery\n"
			elif(users[6]==4)and(users[7]==4):
				readable_result=readable_result+"Prefered Movie genre: Thriller\n"
			elif(users[6]==3)and(users[7]==3):
				readable_result=readable_result+"Prefered Movie genre: Adventure\n"
			elif(users[6]==2)and(users[7]==2):
				readable_result=readable_result+"Prefered Movie genre: Action\n"
			elif(users[6]==1)and(users[7]==1):
				readable_result=readable_result+"Prefered Movie genre: Fantasy\n"
			elif(users[6]==-1)and(users[7]==1):
				readable_result=readable_result+"Prefered Movie genre: Animation\n"
			elif(users[6]==-2)and(users[7]==2):
				readable_result=readable_result+"Prefered Movie genre: Documentary\n"
			elif(users[6]==-3)and(users[7]==3):
				readable_result=readable_result+"Prefered Movie genre: Sci-Fi\n"
			elif(users[6]==-4)and(users[7]==4):
				readable_result=readable_result+"Prefered Movie genre: Western\n"
			elif(users[6]==-5)and(users[7]==5):
				readable_result=readable_result+"Prefered Movie genre: Film-Noir\n"
			elif(users[6]==-6)and(users[7]==6):
				readable_result=readable_result+"Prefered Movie genre: Romance\n"
			elif(users[6]==7)and(users[7]==7):
				readable_result=readable_result+"Prefered Movie genre: Musical\n"
			elif(users[6]==-8)and(users[7]==8):
				readable_result=readable_result+"Prefered Movie genre: Comedy\n"
			elif(users[6]==-9)and(users[7]==9):
				readable_result=readable_result+"Prefered Movie genre: Children's\n"

			if(users[8]==0)and(users[9]==0):
				readable_result=readable_result+"Average Rating genre: Unknown\n"
			elif(users[8]==9)and(users[9]==9):
				readable_result=readable_result+"Average Rating genre: Horror\n"
			elif(users[8]==8)and(users[9]==8):
				readable_result=readable_result+"Average Rating genre: War\n"
			elif(users[8]==7)and(users[9]==7):
				readable_result=readable_result+"Average Rating genre: Drama\n"
			elif(users[8]==6)and(users[9]==6):
				readable_result=readable_result+"Average Rating genre: Crime\n"
			elif(users[8]==5)and(users[9]==5):
				readable_result=readable_result+"Average Rating genre: Mystery\n"
			elif(users[8]==4)and(users[9]==4):
				readable_result=readable_result+"Average Rating genre: Thriller\n"
			elif(users[8]==3)and(users[9]==3):
				readable_result=readable_result+"Average Rating genre: Adventure\n"
			elif(users[8]==2)and(users[9]==2):
				readable_result=readable_result+"Average Rating genre: Action\n"
			elif(users[8]==1)and(users[9]==1):
				readable_result=readable_result+"Average Rating genre: Fantasy\n"
			elif(users[8]==-1)and(users[9]==1):
				readable_result=readable_result+"Average Rating genre: Animation\n"
			elif(users[8]==-2)and(users[9]==2):
				readable_result=readable_result+"Average Rating genre: Documentary\n"
			elif(users[8]==-3)and(users[9]==3):
				readable_result=readable_result+"Average Rating genre: Sci-Fi\n"
			elif(users[8]==-4)and(users[9]==4):
				readable_result=readable_result+"Average Rating genre: Western\n"
			elif(users[8]==-5)and(users[9]==5):
				readable_result=readable_result+"Average Rating genre: Film-Noir\n"
			elif(users[8]==-6)and(users[9]==6):
				readable_result=readable_result+"Average Rating genre: Romance\n"
			elif(users[8]==7)and(users[9]==7):
				readable_result=readable_result+"Average Rating genre: Musical\n"
			elif(users[8]==-8)and(users[9]==8):
				readable_result=readable_result+"Average Rating genre: Comedy\n"
			elif(users[8]==-9)and(users[9]==9):
				readable_result=readable_result+"Average Rating genre: Children's\n"

			if(users[10]==0)and(users[11]==0):
				readable_result=readable_result+"Negative Rating genre: Unknown\n"
			elif(users[10]==9)and(users[11]==9):
				readable_result=readable_result+"Negative Rating genre: Horror\n"
			elif(users[10]==8)and(users[11]==8):
				readable_result=readable_result+"Negative Rating genre: War\n"
			elif(users[10]==7)and(users[11]==7):
				readable_result=readable_result+"Negative Rating genre: Drama\n"
			elif(users[10]==6)and(users[11]==6):
				readable_result=readable_result+"Negative Rating genre: Crime\n"
			elif(users[10]==5)and(users[11]==5):
				readable_result=readable_result+"Negative Rating genre: Mystery\n"
			elif(users[10]==4)and(users[11]==4):
				readable_result=readable_result+"Negative Rating genre: Thriller\n"
			elif(users[10]==3)and(users[11]==3):
				readable_result=readable_result+"Negative Rating genre: Adventure\n"
			elif(users[10]==2)and(users[11]==2):
				readable_result=readable_result+"Negative Rating genre: Action\n"
			elif(users[10]==1)and(users[11]==1):
				readable_result=readable_result+"Negative Rating genre: Fantasy\n"
			elif(users[10]==-1)and(users[11]==1):
				readable_result=readable_result+"Negative Rating genre: Animation\n"
			elif(users[10]==-2)and(users[11]==2):
				readable_result=readable_result+"Negative Rating genre: Documentary\n"
			elif(users[10]==-3)and(users[11]==3):
				readable_result=readable_result+"Negative Rating genre: Sci-Fi\n"
			elif(users[10]==-4)and(users[11]==4):
				readable_result=readable_result+"Negative Rating genre: Western\n"
			elif(users[10]==-5)and(users[11]==5):
				readable_result=readable_result+"Negative Rating genre: Film-Noir\n"
			elif(users[10]==-6)and(users[11]==6):
				readable_result=readable_result+"Negative Rating genre: Romance\n"
			elif(users[10]==7)and(users[11]==7):
				readable_result=readable_result+"Negative Rating genre: Musical\n"
			elif(users[10]==-8)and(users[11]==8):
				readable_result=readable_result+"Negative Rating genre: Comedy\n"
			elif(users[10]==-9)and(users[11]==9):
				readable_result=readable_result+"Negative Rating genre: Children's\n"


			if(users[12]==0)and(users[13]==0):
				readable_result=readable_result+"User's watching tendency for genre: Unknown\n"
			elif(users[12]==9)and(users[13]==9):
				readable_result=readable_result+"User's watching tendency for genre: Horror\n"
			elif(users[12]==8)and(users[13]==8):
				readable_result=readable_result+"User's watching tendency for genre: War\n"
			elif(users[12]==7)and(users[13]==7):
				readable_result=readable_result+"User's watching tendency for genre: Drama\n"
			elif(users[12]==6)and(users[13]==6):
				readable_result=readable_result+"User's watching tendency for genre: Crime\n"
			elif(users[12]==5)and(users[13]==5):
				readable_result=readable_result+"User's watching tendency for genre: Mystery\n"
			elif(users[12]==4)and(users[13]==4):
				readable_result=readable_result+"User's watching tendency for genre: Thriller\n"
			elif(users[12]==3)and(users[13]==3):
				readable_result=readable_result+"User's watching tendency for genre: Adventure\n"
			elif(users[12]==2)and(users[13]==2):
				readable_result=readable_result+"User's watching tendency for genre: Action\n"
			elif(users[12]==1)and(users[13]==1):
				readable_result=readable_result+"User's watching tendency for genre: Fantasy\n"
			elif(users[12]==-1)and(users[13]==1):
				readable_result=readable_result+"User's watching tendency for genre: Animation\n"
			elif(users[12]==-2)and(users[13]==2):
				readable_result=readable_result+"User's watching tendency for genre: Documentary\n"
			elif(users[12]==-3)and(users[13]==3):
				readable_result=readable_result+"User's watching tendency for genre: Sci-Fi\n"
			elif(users[12]==-4)and(users[13]==4):
				readable_result=readable_result+"User's watching tendency for genre: Western\n"
			elif(users[12]==-5)and(users[13]==5):
				readable_result=readable_result+"User's watching tendency for genre: Film-Noir\n"
			elif(users[12]==-6)and(users[13]==6):
				readable_result=readable_result+"User's watching tendency for genre: Romance\n"
			elif(users[12]==7)and(users[13]==7):
				readable_result=readable_result+"User's watching tendency for genre: Musical\n"
			elif(users[12]==-8)and(users[13]==8):
				readable_result=readable_result+"User's watching tendency for genre: Comedy\n"
			elif(users[12]==-9)and(users[13]==9):
				readable_result=readable_result+"User's watching tendency for genre: Children's\n"

			if(users[14]==-1):
				readable_result=readable_result+"User has a preference for movies that generate soft/positive emotions\n"
			elif(users[14]==1):
				readable_result=readable_result+"User has a preference for movies that generate strong and sometimes negative emotions\n"				

			if(users[15]==0):
				readable_result=readable_result+"User is inexperienced judge of movies (movies_rated<=100)\n"
			elif(users[15]==1):
				readable_result=readable_result+"User has small experience in  judging movies (100<movies_rated<=200)\n"
			elif(users[15]==2):
				readable_result=readable_result+"User has moderate experience in  judging movies (200<movies_rated<=300)\n"
			elif(users[15]==3):
				readable_result=readable_result+"User has intermediate experience in  judging movies (300<movies_rated<=400)\n"
			elif(users[15]==4):
				readable_result=readable_result+"User has advanced experience in  judging movies (400<movies_rated<=500)\n"
			elif(users[15]==5):
				readable_result=readable_result+"User is a very experienced judge of movies (500<movies_rated<=600)\n"
			elif(users[15]==6):
				readable_result=readable_result+"User belongs in the most experienced gategory of judging movies (600<movies_rated\n"

			results.write("{0}\n".format(readable_result))
		results.write("-----------------------\n")  
